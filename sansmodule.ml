(* Type Couleur*)
type couleur = Rouge | Noir | DoubleNoir;;
(*
Interprétation du type couleur :
    - Rouge Représente la couleur rouge.
    - Noir Représente La couleur noire.
    - DoubleNoir représente un noire qui serait deux fois plus important.
*)

(* Type abc *)
type abc = Vide | VideNoir | Noeud of int * couleur * abc * abc;;
(*
Interprétation du type abc :
     - Vide représente l'ensemble Vide Ø.
     - VideNoir représente l'ensemble Vide avec la couleur noir pour garder
         l'équilibre de l'arbre.
     - Noeud (racine, _, arbre-gauche, arbre-droit) est l'ensemble
         arbre gauche U arbre droit U {racine}.
*)

(* Preuve (estVide arbre) renvois le booléen (arbre = Ø). *)
let estVide = function
	(* Selon l'interprétation, Vide représente l'ensemble vide Ø. *)
	| Vide -> true
	(* Dans les autres cas, arbre = Noeud(racine, _, _, _)
         Par réfinition, l'ensemble représenté par l'arbre contient au moin
         la racine, et ainsi est non vide. *)
	| _    -> false;;

(*
	Preuvre (estDans e arbre) renvois le booléen (x dans arbre).
	Hypothése : arbre est un arbre binaire de recherche.
	Par induction sur la structure des valeurs du type abc.
*)
let rec estDans e = function
	(* Base de l'induction : selon l'interprétation, Vide représente
	l'ensemble vide. *)
	| Vide -> false;
	(*
		Cas particulier : arbre = Noeud(r, _, _, _).
		Par définition, l'ensemble représenter contient r, qui est
		égale à e que on cherche.
		Alors e appartient à l'ensemble représenter par l'arbre.
	*)
	| Noeud(r, _, _, _)  when e = r -> true
	(*
		Supposons arbre = Noeud(r, _, _, _) et r < e.
		On a avec l'hypothése que l'arbre est binaire de recherche,
		toute les valeurs de ad sont donc inférieur à r et toute les
		valeurs de ag sont supérieur.
		Comme r est strictement supérieur à e, e ne peux pas appartenir
		à ad U {r}.
		e n'appartient à l'arbre alors si et seulement si il appartient
		à ag.
		Selon l'hypothése d'induction, pour tout arbre a' d'une hauteur
		strictement inférieur à celle de l'arbre, e appartient à a' si
		et seulement si (estDans e a') renvois vrai.
		Alors, puisque ag est un sous-arbre de l'arbre, x appartient à
		ag si et seulement si (estDans e ag) renvois vrais.
	*)
	| Noeud(r, _, ag, _) when e < r -> estDans e ag
	(* Cas symétrique que le cas précédent. *)
	| Noeud(r, _, _, ad) when e > r -> estDans e ad;;

(*
Preuve (racineestrouge arbre) renvois le booléen (racine est rouge).
*)
let racineestrouge = function
	(*
		Par définition, l'ensemble représenter à une racine de couleur
		rouge, se qui est se que on chercher.
		Alors la racine est rouge.
	*)
	| Noeud(_, Rouge, _, _) -> true
	(*
		Par définition si la racine est pas rouge (racine rouge) est
		forcément faux.
	*)
	| _ -> false;;

(*
	Preuve (colorer racine c arbre) renvois l'arbre avec la racine
	colorer avec la couleur fournis.
*)
let colorer_racine c = function
	(*
		Avec a = Noeud(r, color, ag, ad).
		Par définition l'ensemble a avec la couleur c est
		Noeud(r, c, ag, ad).
	*)
	| Noeud(r, _, ag, ad) -> Noeud(r, c, ag, ad)
	(*
		Selon l'interprétation VideNoir est l'ensemble vide Ø.
	*)
	| VideNoir -> VideNoir
	(*
		Selon l'interprétation Vide est l'ensemble vide Ø
		qui na pas de couleur.
	*)
	| Vide -> Vide;;

(*
	Preuve (fils_rouge a) renvois le bouléen (un fils est rouge).
*)
let fils_rouge = function
	(*
		Sois a = Noeud(_, _, Noeud(_, Rouge, _, _), _).
		Le fils gauche de a est rouge, par définition alors un fils
		de a est rouge.
	*)
	| Noeud(_, _, Noeud(_, Rouge, _, _), _) -> true
	(*
		De maniére similaire le fils droit de a est rouge.
	*)
	| Noeud(_, _, _, Noeud(_, Rouge, _, _)) -> true
	(*
		Selon l'interprétation d'abc, a na que deux fils, ag et ad,
		le cas ag rouge et le cas ad rouge sont traiter avant.
		Alors on a ag non rouge et ad non rouge seul fils de a, a na
		donc aucun fils rouge.
		Donc (un fils est rouge) est faux.
	*)
	| _ -> false;;

(*
  Preuve (rotation_droite a) renvois a' tel que a c a' et a' c a, le sous arbre
  gauche est remonter tanti que le droit se fais redécendre en restant un arbre
  binaire de recherche si il l'était.
*)
let rotation_droite = function
  (*
    Cas particulier a = Noeud(n, couln, Noeud(m, coulm, a1, a2), a3)
    Pour remonter {m, coulm} il faut faire déscendre {n, couln}, n < a3, n > m,
    on ne peux pas passer n à gauche pour garder l'arbre de recherche, alors on
    déplace de maniére à garder l'équilibre a1.
    a Noeud(m, coulm, a1, Noeud(n, couln, a2, a3))
  *)
	| Noeud(n, couln, Noeud(m, coulm, a1, a2), a3) -> Noeud(m, coulm, a1, Noeud(n, couln, a2, a3))
  (*
    Autre cas ou a = Noeud(n, couln, d, c) ou c et d est vide.
    La rotation à droite na aucun éffét.
  *)
	| arbre -> arbre;;

(* De maniére symétrique. *)
let rotation_gauche = function
	| Noeud(m, colm, a1, Noeud(n, coln, a2, a3)) -> Noeud(n, coln, Noeud(m, colm, a1, a2), a3)
	| arbre -> arbre;;

(*
  Preuve (equilibrer a) renvois a', a équilibrer.
  Hypothése : arbre est un arbre binaire bicolore de recherche.
	Par induction sur la structure des valeurs du type abc.
*)
let rec equilibrer = function
	| Noeud(g, Noir, (Noeud(_, Rouge, _, _ ) as p), (Noeud(_, Rouge, _, _ ) as f))
			when fils_rouge p || fils_rouge f ->
			Noeud (g, Rouge, colorer_racine Noir p, colorer_racine Noir f)
	(* | -> rotation_droite f soit noir soit Vide*)
	| Noeud(g, Noir, (Noeud(i, Rouge, x, adp)), f) when racineestrouge x ->
			rotation_droite(Noeud(g, Rouge, Noeud(i, Noir, x, adp), f))
	(* | -> rotation_gauche *)
	| Noeud(g, Noir, (Noeud(_, Rouge, _, x) as p), f) when racineestrouge x ->
			equilibrer(Noeud(g, Noir, rotation_gauche(p), f))
	(* | -> rotation_droite *)
	| Noeud(g, Noir, f, (Noeud(_, Rouge, x, _) as p)) when racineestrouge x ->
			equilibrer(Noeud(g, Noir, f, rotation_droite(p)))
	(* | -> rotation_gauche *)
	| Noeud(g, Noir, f, (Noeud(i, Rouge, agp, x))) when racineestrouge x ->
			rotation_gauche(Noeud(g, Rouge, f, Noeud(i, Noir, agp, x)))
	| arbre -> arbre;;
(* 4 cas def 3 + identité *)

(*
  Preuve (inserer x arbre) renvois arbre  U x.
  Hypothése : arbre est un arbre binaire bicolore de recherche.
	Par induction sur la structure des valeurs du type abc.
*)
let inserer x arbre =
	let rec auxinsertion x = function
  (*
    Cas de base a = Vide.
    Selon l'interprétation on veux avoir un arbre binaire de recherche bicolore,
    la racine dois donc forcément être rouge.
  *)
	| Vide -> Noeud(x, Rouge, Vide, Vide)
  (*
    Cas particulier inserer dans un arbre VideNoir.
    Par interprétation la pratique est impossible.
  *)
  | VideNoir -> invalid_arg "Videnoir !!!"
  (*
    Cas particulier a = Noeud(r, _, ad, ag) et r = x
    r est dans a, par définition alors x est déjà dans r, il n'y à rien à faire.
  *)
	| Noeud(r, _, _, _) as a when x = r -> a
  (*
    Cas particulier a = Noeud(r, c, ag, ad) and r > x
    Par l'hypothése de recursion a est un arbre binaire, alors x si il existe
    ou si il doit être placer devra se trouver dans ag car tout les élément de
    ag sont inférieur à x, l'insertion d'un élément va provoquer le changement
    de l'équilibre.
  *)
	| Noeud(r, c, ag, ad) when x < r -> equilibrer (Noeud(r, c, auxinsertion x ag, ad))
  (*
    Cas particulier symétrique avec r < x
  *)
	| Noeud(r, c, ag, ad) -> equilibrer (Noeud(r, c, ag, auxinsertion x ad))
	in colorer_racine Noir (auxinsertion x arbre);;

let conflict = function
	| Noeud(x3, Noir, Noeud(x2, Rouge, Noeud(x1, Rouge, t1, t2), t3), t4) ->
		Noeud(x2, Rouge, Noeud(x1, Noir, t1, t2), Noeud(x3, Noir, t3, t4))
	| Noeud(x3, Noir, Noeud(x1, Rouge, t1,  Noeud(x2, Rouge, t2, t3)), t4) ->
		Noeud(x2, Rouge, Noeud(x1, Noir, t1, t2), Noeud(x3, Noir, t3, t4))
	| Noeud(x1, Noir, t1, Noeud(x3, Rouge, Noeud(x2, Rouge, t2, t3), t4)) ->
		Noeud(x2, Rouge, Noeud(x1, Noir, t1, t2), Noeud(x3, Noir, t3, t4))
	| Noeud(x1, Noir, t1, Noeud(x2, Rouge, t2, Noeud(x3, Rouge, t3, t4))) ->
		Noeud(x2, Rouge, Noeud(x1, Noir, t1, t2), Noeud(x3, Noir, t3, t4))
	| arbre -> arbre;;

let unbalanced_left = function
	| Noeud(x1, Rouge, t1, Noeud(x2, Noir, t2, t3)) ->
		conflict (Noeud(x1, Noir, t1, Noeud(x2, Rouge, t2, t3))), false
	| Noeud(x1, Noir, t1, Noeud(x3, Rouge, Noeud(x2, Noir, t2, t3), t4)) ->
		conflict (Noeud(x3, Noir, Noeud(x1, Noir, t1, Noeud(x2, Rouge, t2, t3)), t4)), false
	| Noeud(x1, Noir, t1, Noeud(x2, Noir, t2, t3)) ->
		conflict (Noeud(x1, Noir, t1, Noeud(x2, Rouge, t2, t3))), false
	| arbre -> (arbre, false);;

let unbalanced_right = function
	| Noeud(x2, Rouge, Noeud(x1, Noir, t1, t2), t3) ->
		conflict (Noeud(x2, Noir, Noeud(x1, Rouge, t1, t2), t3)), false
	| Noeud(x3, Noir, Noeud(x1, Rouge, t1, Noeud(x2, Noir, t2, t3)), t4) ->
		conflict (Noeud(x1, Noir, t1, Noeud(x3, Noir, Noeud(x2, Rouge, t2, t3), t4))), false
	| Noeud(x2, Noir, Noeud(x1, Noir, t1, t2), t3) ->
		conflict (Noeud(x2, Noir, Noeud(x1, Rouge, t1, t2), t3)), true
	| arbre -> (arbre, false);;

(*
  Preuve (min_tree a) renvois la plus petite valeurs de a.
  Hypothése : arbre est un arbre binaire de recherche.
	Par induction sur la structure des valeurs du type abc.
*)
let rec min_tree = function
  (*
    Cas de base, a = Noeud(x, _, Vide, _).
    Par hypothése d'induction a est un arbre binaires de recherche, toute les
    valeurs de ad sont donc inférieur à x et toute les valeurs de ag le sont
    supérieur, or ad est vide, par définition x est donc la plus petite valeurs
    de a.
  *)
	| Noeud(x, _, Vide, _) -> x
  (*
    Suposon a = Noeud(x, _, ad, ag)
    Par hypothése d'induction a est un arbre binaires de recherche, toute les
    valeurs de ad sont donc inférieur à x et toute les valeurs de ag le sont
    supérieur.
    Le minimum de a se trouve dans forcément dans ad, alors il sera donner par
    (min_tree ad).
  *)
	| Noeud(_, _, t, _) -> min_tree t
  (*
    Cas particulier restant, arbre vide, par l'implémentation Vide représente
    l'ensemble vide, par définition l'ensemble na pas de minimum, la fonction
    léve donc une érreur.
  *)
	| _ -> invalid_arg "min_tree";;

(*
  Preuve (max_tree a) renvois la plus gra,de valeurs de a.
  Hypothése : arbre est un arbre binaire de recherche.
	Par induction sur la structure des valeurs du type abc.
*)
let rec max_tree = function
  (*
    Cas de base, a = Noeud(x, _, Vide, _).
    Par hypothése d'induction a est un arbre binaires de recherche, toute les
    valeurs de ad sont donc inférieur à x et toute les valeurs de ag le sont
    supérieur, or ad est vide, par définition x est donc la plus petite grande
    de a.
  *)
	| Noeud(x, _, _, Vide) -> x
  (*
    Suposon a = Noeud(x, _, ad, ag)
    Par hypothése d'induction a est un arbre binaires de recherche, toute les
    valeurs de ad sont donc inférieur à x et toute les valeurs de ag le sont
    supérieur.
    Le maximum de a se trouve dans forcément dans ag, alors il sera donner par
    (max_tree ag).
  *)
	| Noeud(_, _, _, t) -> max_tree t
  (*
    Cas particulier restant, arbre vide, par l'implémentation Vide représente
    l'ensemble vide, par définition l'ensemble na pas de maximum, la fonction
    léve donc une érreur.
  *)
	| _ -> invalid_arg "max_tree";;

let rec remove_aux x = function
	| Vide -> Vide, false
	| Noeud(y, Rouge, Vide, Vide) when x = y -> Vide, false
	| Noeud(y, Noir, Vide, Vide) when x = y -> Vide, true
	| Noeud(y, c, Vide, t2) when x = y ->
		let y' = min_tree t2 in
		let t2', o = remove_aux y' t2 in
		let t' = Noeud(y', c, Vide, t2') in
		if o then unbalanced_right t' else (conflict t', false)
	| Noeud(y, c, t1, t2) when x = y ->
		let y' = max_tree t1 in
		let t1', o = remove_aux y' t1 in
		let t' = Noeud(y', c, t1', t2) in
		if o then unbalanced_left t' else (conflict t', false)
	| Noeud(y, c, t1, t2) when x < y ->
		let t1', o = remove_aux x t1 in
		let t' = Noeud(y, c, t1', t2) in
		if o then unbalanced_left t' else (conflict t', false)
	| Noeud(y, c, t1, t2) (* when x > y *) ->
		let t2', o = remove_aux x t2 in
		let t' = Noeud(y, c, t1, t2') in
		if o then unbalanced_right t' else (conflict t', false)
	| _ -> invalid_arg "remove_aux";;

let supprimer x arb =
	let arb', _ = remove_aux x arb in
	colorer_racine Noir arb';;

let rec min = min_tree;;

let rec max = max_tree;;

let exploser = function
	| Noeud(x, _, ag, ad) -> x, colorer_racine Noir ag, colorer_racine  Noir ad
	| _ -> invalid_arg "découper_selon_min";;

let union a = function
	| Vide -> a
	| b -> let rec union_aux acc = function
		| [] -> acc
		| Vide::l -> union_aux acc l
		| b::l -> let (r, ag, ad) = exploser b
		   in union_aux (inserer r acc) (ag::ad::l)
		in union_aux a [b];;

let intersection a b =
	let rec interaux a b acc = match a, b with
	| Vide, _ -> acc
	| _ , Vide -> acc
	| Noeud(x, _, ag, ad), b ->
		if estDans x b
		then interaux (supprimer x a) (supprimer x b) (inserer x acc)
		else interaux (supprimer x a) b acc
	| _ -> invalid_arg "erreur inter"
	in interaux a b Vide;;

(*
  Preuve difference a b) renvois un arbre (a ∩ b)
  Hypothése : arbre est un arbre binaire de recherche.
  Par induction sur la structure des valeurs du type abc.
*)
let rec difference a b = match a, b with
  (*
    Cas particulier a = Vide
    Selon l'interprétation Vide représente l'ensemble vide Ø.
    Par définition a ∩ vide = vide.
  *)
	| Vide, _ -> Vide
  (*
    Suposon a = Noeud(x, c, ag, ad) et b = Noeud(x', c', ag', ad').
    Avec l'hypothése que a et b sont des arbres binaires de recherche
    Si x c a alors par définition (a ∩ b) ne posséde pas x.
    (a ∩ b) sera alors renvoyer par (difference a\x b\x).
    Sinon x se trouve bien dans (a ∩ b) et la différent sera alors donner par
    (difference a b\x).
  *)
	| a, Noeud(x, _, _, _) ->
		if estDans x a
		then difference (supprimer x a) (supprimer x b)
		else difference a (supprimer x b)
  (*
    Cas particulier a ni un Noeud ni vide.
    a videNoire, ne peux pas calculer de différence.
  *)
	| _ -> invalid_arg "erreur diff";;

let differenceSym a b =
	let rec diff_sym_aux a b acc = match a, b with
	| Vide, Vide -> acc
	| Vide, (Noeud(x, _, _, _) as b) -> diff_sym_aux Vide (supprimer x b) (inserer x acc)
	| Noeud(x, _, ag, ad), b ->
		if estDans x b
		then diff_sym_aux (supprimer x a) (supprimer x b) acc
		else diff_sym_aux (supprimer x a) b (inserer x acc)
	| _ -> invalid_arg "erreur diff_sym"
	in diff_sym_aux a b Vide;;

let ensemble_vers_liste ens =
	let rec e_vers_l_aux ens liste = match ens with
	| Vide -> liste
	| Noeud(_) as arb -> e_vers_l_aux (supprimer (max arb) arb) ((max arb)::liste)
	| _ -> invalid_arg "erreur ens_vers_liste"
	in e_vers_l_aux ens [];;

let liste_vers_ensemble l =
	List.fold_right inserer l Vide;;

(*
  Preuve (est_inclus_dans a b) renvois une booléen qui est (a c b).
  Hypothése : a et b sont des arbre binaire de recherche.
  Par induction sur la structure des valeurs du type abc.
*)
let rec est_inclus_dans a b = match a, b with
  (*
    Base de l'induction : selon l'interprétation, Vide représente l'ensemble
    vide, par définition l'ensemble vide se trouve dans tout les ensemble.
  *)
	| Vide, _ -> true
  (*
    Suposon a = Noeud(x, c, ag, ad) et b = abc.
    Avec l'hypothése que a et b sont des arbres binaires de recherche
    si (estDans x b) renvois vrais alors x est dans b, il ne reste qua vérifier
    le reste sinon x n'est pas dans b alors a n'est pas inclus dans b.
    Selon l'hypothése d'induction, pour tout arbre a' d'une hauteur
		strictement inférieur à celle de l'arbre, alors pour a c b il faut
    ag c b et ad c b, soit (est_inclus_dans ag b) et (est_inclus_dans ad b).
  *)
	| Noeud(x, _, ag, ad), b ->
		if estDans x b then est_inclus_dans ag b && est_inclus_dans ad b
		else false
  (*
    Cas particulier a n'est ni un Noeud ni vide, cas non gérable.
  *)
	| _ -> invalid_arg "erreur est_inclus_dans";;

(*
  Preuve (est_egal a b) renvois un booléen (a = b)
*)
let est_egal a b =
  (*
    Par définition a = b est équivalent à a c b oui b c a, or
    (est_inclus_dans a b) revois un booléen vrais si a c b donc
    a = b est équivalent à ( (est_inclus_dans a b) && (est_inclus_dans b a)).
  *)
	if est_inclus_dans a b && est_inclus_dans b a then true
  (*
    Dans les autres cas, a n'est pas dans b ou b n'est pas dans a.
      Par définition alors a != b.
  *)
	else false;;

(*
	Preuve (card a) renvois le nombre de Noeud.
	Hypothése : arbre est un arbre binaire de recherche.
	Par induction sur la structure des valeurs du type abc.
*)
let rec card = function
	(* Base de l'induction : selon l'interprétation, Vide représente
	l'ensemble vide, par définition l'ensemble vide na aucun élément. *)
	| Vide -> 0
	(*
		Cas particulier : arbre = VideNoir.
		Selon l'interprétation VideNoir représente aussi l'ensemble
		vide.
	*)
	| VideNoir -> 0
	(*
  Supposons arbre = Noeud(r, _, ag, ad).
    On a avec l'hypothése que l'arbre est binaire de recherche,
    Il y a donc des valeurs dans les sous ensemble ag et ad.
    Toute les valeurs de ad sont donc inférieur à r et toute les
		valeurs de ag sont supérieur les valeurs sont donc toute différent de r et
    les deux ensemble sont distincs.
    Selon l'hypothése d'induction, pour tout arbre a' d'une hauteurs strinctement
    inférieur à celle de l'arbre, (card ag) renvois (card arbre) - 1 - (card ad)
	*)
	| Noeud(_, _, ag, ad) -> 1 + card ag + card ad;;

(* Type Contexte*)
type contexte = CtxtVide | CtxtGauche of contexte * int * couleur * abc
												 | CtxtDroite of contexte * int * couleur * abc;;
(*
Interprétation du type Contexte :
    - CtxVide représente l'ensemble Vide Ø.
    - CtxtGauche(cxt, element, couleur, arbre) est l'ensemble
        Arbre U {racine} U {contexte} composer avec l'arbre à
        droite.
    - Même pour CtxDroite de maniére symétrique.
*)

(* Type config *)
type config = contexte * abc;;
(*
Interprétation du type config :
    - config {contexte} U {abc}.
    - Représente l'ensemble vide quand contexte et abc le représente aussi.
*)

(* Preuve (dsctgauche config) renvois (ctxt U {racine} U ad, ag)*)
let dsctgauche = function
	(*
		Suposon a = (ctxt, Noeud(v, c, ag, ad)).
		Selon l'interprétation on a (v U c) la racine.
		Par définition on a alors (ctxt U {v} U {c} U ad, ag).
	*)
	| (ctxt, Noeud(v, c, ag, ad)) -> (CtxtGauche(ctxt, v, c, ad), ag)
	(*
		Suposon a = (ctxt, abc).
		On a abc qui n'est pas un noeud selon l'étape présédente, par
		l'interprétation on a alors abc Vide ou VideNoire, qui sont
		encore par l'interprétation l'ensemble vide Ø.
		Par définition l'ensemble vide ne poséde fils on à alors
		(ctxt, Vide).
	*)
	| _ -> failwith "erreur";;

(* Preuve (dsctdroite config) renvois (ctxt U {racine} U ag, ad)*)
let dsctdroite = function
	(*
		Suposon a = (ctxt, Noeud(v, c, ag, ad)).
		Selon l'interprétation on a (v U c) la racine.
		Par définition on a alors (ctxt U {v} U {c} U ag, ad).
	*)
	| (ctxt, Noeud(v, c, ag, ad)) -> (CtxtDroite(ctxt, v, c, ag), ad)
	(*
		Suposon a = (ctxt, abc).
		On a abc qui n'est pas un noeud selon l'étape présédente, par
		l'interprétation on a alors abc Vide ou VideNoire, qui sont
		encore par l'interprétation l'ensemble vide Ø.
		Par définition l'ensemble vide ne poséde fils on à alors
		(ctxt, Vide).
	*)
	| _ -> (ctxt, Vide);;

(*
	Preuve (remontee config) renvois renvois l'arbre construit avec
	l'union de l'abc et du contexte.
*)
let remontee = function
	(*
		Selon l'interprétation CtxVide représente l'ensemble vide Ø.
		Par définition Ø U x = x.
		Donc CtxtVide U a = a.
	*)
	| (CtxtVide, a) -> a
	(*
		Cas contexte est CtxtGauche
		Selon l'interprétation alors l'arbre dois être placer à gauche
		de l'arbre présent dans le context.
		Et x, c, abr contexte.
		On a donc (ctxt, (x U c) U arb U arbre)
	*)
	| (CtxtGauche(ctxt, x, c, abr), arbre) -> (ctxt, Noeud(x, c, arbre, abr))
	(*
		Cas contexte est CtxtGauche
		Selon l'interprétation alors l'arbre dois être placer à droite
		de l'arbre présent dans le context.
		Et x, c, abr contexte.
		On a donc (ctxt, arbre U (x U c) U arb)
	*)
	| (CtxtDroite(ctxt, x, c, abr), arbre) -> (ctxt, Noeud(x, c, abr, arbre));;

let dsctgauche = function
	| (ctxt, Noeud(_, _, _, _)) -> true
	| _ -> failwith "erreur";;

let dsctdroite = function
	| (ctxt, Noeud(_, _, _, _)) -> true
	| _ -> failwith "erreur";;

let remontee = function
	| (ctxt, Noeud(_, _, _, _)) -> true
	| _ -> failwith "erreur";;

