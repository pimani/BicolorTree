module type TypeOrdonne =
  sig
    type t
    val compare : t -> t -> int
  end

module EntierOrdreNaturel:TypeOrdonne with type t = int =
  struct
    type t = int
    let compare = (-)
  end

module type Ensemble =
  sig
    type element
    type abc
    val vide : abc
    val estVide : abc -> bool
    val estDans : element -> abc -> bool
    val inserer : element -> abc -> abc
    val supprimer : element -> abc -> abc
    (* val supprimerMax : abc -> element * abc *)
    val union : abc -> abc -> abc
    val intersection : abc -> abc -> abc
    val difference : abc -> abc -> abc
    val differenceSym : abc -> abc -> abc
    val ensemble_vers_liste : abc -> element list
    val liste_vers_ensemble  : element list -> abc
    val est_inclus_dans : abc -> abc -> bool
    val est_egal : abc -> abc -> bool
    (* val appliquer_sur : (element -> 'a -> 'a) -> abc -> 'a -> 'a *)
    val card : abc -> int
    (* val est_verifie_par_tous : (element -> bool) -> abc -> bool  *)
    (* val est_verifie_par_un : (element -> bool) -> abc -> bool    *)
    (* val filtrer_selon : (element -> bool) -> abc -> abc          *)
    (* val separer_selon : (element -> bool) -> abc -> abc * abc    *)
    (* val separer_selon_pivot : element -> abc -> abc * bool * abc *)
  end

module MakeEnsemble (O : TypeOrdonne) : Ensemble  with type element = O.t =
  struct
    type element = O.t
    type couleur = Rouge | Noir | DoubleNoir;;

type abc = Vide | VideNoir | Noeud of element * couleur * abc * abc;;
let comp = O.compare;;
let ($=$) x y = comp x y = 0;;
let ($<$) x y = comp x y < 0;;
let ($>$) x y = comp x y > 0;;
let ($&&$) f g x = f x && g x;;
let ($||$) f g x = f x || g x;;

let vide = Vide;;

type contexte = CtxtVide | CtxtGauche of contexte * element * couleur * abc 
												 | CtxtDroite of contexte * element * couleur * abc;;

type config = contexte * abc;;


let dsctgauche = function
	| (ctxt, Noeud(v, c, ag, ad)) -> (CtxtGauche(ctxt, v, c, ad), ag)
	| _ -> failwith "erreur";;

let dsctdroite = function
	| (ctxt, Noeud(v, c, ag, ad)) -> (CtxtDroite(ctxt, v, c, ag), ad)
	| _ -> failwith "erreur";;

let remontee = function
| (CtxtVide, _) -> failwith "Ne peut pas remonter, contexte vide."
| (CtxtGauche(ctxt, x, c, abr), arbre) -> (ctxt, Noeud(x, c, arbre, abr))
| (CtxtDroite(ctxt, x, c, abr), arbre) -> (ctxt, Noeud(x, c, abr, arbre));;

let dsctgauche = function
	| ctxt * Noeud(_, _, _, _) -> true
	| _ -> failwith "erreur";;

let dsctdroite = function
	| ctxt * Noeud(_, _, _, _) -> true
	| _ -> failwith "erreur";;

let remontée = function
	| ctxt * Noeud(_, _, _, _) -> true
	| _ -> failwith "erreur";;

let estVide = function
	| Vide -> true
	| _    -> false;;

let rec estDans e = function
	| Noeud(r, _, _, _)  when e $=$ r -> true
	| Noeud(r, _, ag, _) when e $<$ r -> estDans e ag
	| Noeud(r, _, _, ad) when e $>$ r -> estDans e ad
	| _ -> false;;

let racineestrouge = function
	| Noeud(_, Rouge, _, _) -> true
	| _ -> false;;

let colorer_racine c = function
	| Noeud(r, _, ag, ad) -> Noeud(r, c, ag, ad)
	| VideNoir -> VideNoir
	| Vide -> Vide;;

let fils_rouge = function
	| Noeud(_, _, Noeud(_, Rouge, _, _), _) -> true
	| Noeud(_, _, _, Noeud(_, Rouge, _, _)) -> true
	| _ -> false;;

let rotation_droite = function
	| Noeud(n, coln, Noeud(m, colm, a1, a2), a3) -> Noeud(m, colm, a1, Noeud(n, coln, a2, a3))
	| arbre -> arbre;;

let rotation_gauche = function
	| Noeud(m, colm, a1, Noeud(n, coln, a2, a3)) -> Noeud(n, coln, Noeud(m, colm, a1, a2), a3)
	| arbre -> arbre;;

let rec equilibrer = function
	| Noeud(g, Noir, (Noeud(_, Rouge, _, _ ) as p), (Noeud(_, Rouge, _, _ ) as f)) 
			when fils_rouge p || fils_rouge f -> 
			Noeud (g, Rouge, colorer_racine Noir p, colorer_racine Noir f)
	(* | -> rotation_droite f soit noir soit Vide*)
	| Noeud(g, Noir, (Noeud(i, Rouge, x, adp)), f) when racineestrouge x -> 
			rotation_droite(Noeud(g, Rouge, Noeud(i, Noir, x, adp), f))
	(* | -> rotation_gauche *)
	| Noeud(g, Noir, (Noeud(_, Rouge, _, x) as p), f) when racineestrouge x -> 
			equilibrer(Noeud(g, Noir, rotation_gauche(p), f))
	(* | -> rotation_droite *)
	| Noeud(g, Noir, f, (Noeud(_, Rouge, x, _) as p)) when racineestrouge x -> 
			equilibrer(Noeud(g, Noir, f, rotation_droite(p)))
	(* | -> rotation_gauche *)
	| Noeud(g, Noir, f, (Noeud(i, Rouge, agp, x))) when racineestrouge x -> 
			rotation_gauche(Noeud(g, Rouge, f, Noeud(i, Noir, agp, x)))
	| arbre -> arbre;;
(* 4 cas def 3 + identité *)

let inserer x arbre =
	let rec auxinsertion x = function
	| VideNoir -> invalid_arg "Videnoir !!!"
	| Vide -> Noeud(x, Rouge, Vide, Vide)
	| Noeud(r, _, _, _) as a when x $=$ r -> a
	| Noeud(r, c, ag, ad) when x $<$ r -> equilibrer (Noeud(r, c, auxinsertion x ag, ad))
	| Noeud(r, c, ag, ad) -> equilibrer (Noeud(r, c, ag, auxinsertion x ad))
	in colorer_racine Noir (auxinsertion x arbre);;

let conflict = function
	| Noeud(x3, Noir, Noeud(x2, Rouge, Noeud(x1, Rouge, t1, t2), t3), t4) -> 
		Noeud(x2, Rouge, Noeud(x1, Noir, t1, t2), Noeud(x3, Noir, t3, t4))
	| Noeud(x3, Noir, Noeud(x1, Rouge, t1,  Noeud(x2, Rouge, t2, t3)), t4) -> 
		Noeud(x2, Rouge, Noeud(x1, Noir, t1, t2), Noeud(x3, Noir, t3, t4))
	| Noeud(x1, Noir, t1, Noeud(x3, Rouge, Noeud(x2, Rouge, t2, t3), t4)) -> 
		Noeud(x2, Rouge, Noeud(x1, Noir, t1, t2), Noeud(x3, Noir, t3, t4))
	| Noeud(x1, Noir, t1, Noeud(x2, Rouge, t2, Noeud(x3, Rouge, t3, t4))) -> 
		Noeud(x2, Rouge, Noeud(x1, Noir, t1, t2), Noeud(x3, Noir, t3, t4))
	| arbre -> arbre;;

let unbalanced_left = function
	| Noeud(x1, Rouge, t1, Noeud(x2, Noir, t2, t3)) ->
		conflict (Noeud(x1, Noir, t1, Noeud(x2, Rouge, t2, t3))), false
	| Noeud(x1, Noir, t1, Noeud(x3, Rouge, Noeud(x2, Noir, t2, t3), t4)) ->
		conflict (Noeud(x3, Noir, Noeud(x1, Noir, t1, Noeud(x2, Rouge, t2, t3)), t4)), false
	| Noeud(x1, Noir, t1, Noeud(x2, Noir, t2, t3)) ->
		conflict (Noeud(x1, Noir, t1, Noeud(x2, Rouge, t2, t3))), false
	| arbre -> (arbre, false);;

let unbalanced_right = function
	| Noeud(x2, Rouge, Noeud(x1, Noir, t1, t2), t3) ->
		conflict (Noeud(x2, Noir, Noeud(x1, Rouge, t1, t2), t3)), false		
	| Noeud(x3, Noir, Noeud(x1, Rouge, t1, Noeud(x2, Noir, t2, t3)), t4) ->
		conflict (Noeud(x1, Noir, t1, Noeud(x3, Noir, Noeud(x2, Rouge, t2, t3), t4))), false
	| Noeud(x2, Noir, Noeud(x1, Noir, t1, t2), t3) ->
		conflict (Noeud(x2, Noir, Noeud(x1, Rouge, t1, t2), t3)), true
	| arbre -> (arbre, false);;

let rec min_tree = function
	| Noeud(x, _, Vide, _) -> x
	| Noeud(_, _, t, _) -> min_tree t 
	| _ -> invalid_arg "min_tree";;

let rec max_tree = function
	| Noeud(x, _, _, Vide) -> x
	| Noeud(_, _, _, t) -> max_tree t
	| _ -> invalid_arg "max_tree";;

let rec remove_aux x = function
	| Vide -> Vide, false
	| Noeud(y, Rouge, Vide, Vide) when x = y -> Vide, false
	| Noeud(y, Noir, Vide, Vide) when x = y -> Vide, true
	| Noeud(y, c, Vide, t2) when x = y -> 
		let y' = min_tree t2 in
		let t2', o = remove_aux y' t2 in
		let t' = Noeud(y', c, Vide, t2') in
		if o then unbalanced_right t' else (conflict t', false)
	| Noeud(y, c, t1, t2) when x = y -> 
		let y' = max_tree t1 in
		let t1', o = remove_aux y' t1 in
		let t' = Noeud(y', c, t1', t2) in
		if o then unbalanced_left t' else (conflict t', false)
	| Noeud(y, c, t1, t2) when x < y -> 
		let t1', o = remove_aux x t1 in
		let t' = Noeud(y, c, t1', t2) in
		if o then unbalanced_left t' else (conflict t', false)
	| Noeud(y, c, t1, t2) (* when x > y *) -> 
		let t2', o = remove_aux x t2 in
		let t' = Noeud(y, c, t1, t2') in
		if o then unbalanced_right t' else (conflict t', false)
	| _ -> invalid_arg "remove_aux";;

let supprimer x arb =
	let arb', _ = remove_aux x arb in
	colorer_racine Noir arb';;

let rec min = min_tree;;

let rec max = max_tree;;

let exploser = function
	| Noeud(x, _, ag, ad) -> x, colorer_racine Noir ag, colorer_racine  Noir ad
	| _ -> invalid_arg "découper_selon_min";;

(* Exo 2 *)
let union a = function
	| Vide -> a
	| b -> let rec union_aux acc = function
		| [] -> acc
		| Vide::l -> union_aux acc l
		| b::l -> let (r, ag, ad) = exploser b
		   in union_aux (inserer r acc) (ag::ad::l)
		in union_aux a [b];;

let intersection a b = 
	let rec interaux a b acc = match a, b with
	| Vide, _ -> acc
	| _ , Vide -> acc
	| Noeud(x, _, ag, ad), b -> 
		if estDans x b 
		then interaux (supprimer x a) (supprimer x b) (inserer x acc)
		else interaux (supprimer x a) b acc
	| _ -> invalid_arg "erreur inter"
	in interaux a b Vide;;

let rec difference a b = match a, b with 
	| Vide, _ -> Vide
	| a, Noeud(x, _, _, _) ->
		if estDans x a
		then difference (supprimer x a) (supprimer x b)
		else difference a (supprimer x b)
	| _ -> invalid_arg "erreur diff";;

let differenceSym a b = 
	let rec diff_sym_aux a b acc = match a, b with
	| Vide, Vide -> acc
	| Vide, (Noeud(x, _, _, _) as b) -> diff_sym_aux Vide (supprimer x b) (inserer x acc)
	| Noeud(x, _, ag, ad), b -> 
		if estDans x b 
		then diff_sym_aux (supprimer x a) (supprimer x b) acc
		else diff_sym_aux (supprimer x a) b (inserer x acc)
	| _ -> invalid_arg "erreur diff_sym"
	in diff_sym_aux a b Vide;;

(* Exo 3 *)
let liste_vers_ensemble l =
	List.fold_right inserer l Vide;;

let ensemble_vers_liste ens = 
	let rec e_vers_l_aux ens liste = match ens with
	| Vide -> liste
	| Noeud(_) as arb -> e_vers_l_aux (supprimer (max arb) arb) ((max arb)::liste)
	| _ -> invalid_arg "erreur ens_vers_liste"
	in e_vers_l_aux ens [];;

(* Exo 4 *)
let rec est_inclus_dans a b = match a, b with
	| Vide, _ -> true
	| Noeud(x, _, ag, ad), b -> 
		if estDans x b then est_inclus_dans ag b && est_inclus_dans ad b
		else false 
	| _ -> invalid_arg "erreur est_inclus_dans";;

let est_egal a b = 
	if est_inclus_dans a b && est_inclus_dans b a then true
	else false;;

let rec card = function
	| VideNoir -> 0
	| Vide -> 0
	| Noeud(_, _, ag, ad) -> 1 + card ag + card ad;;

  end

module EnsEntier = MakeEnsemble (EntierOrdreNaturel) ;;
open EnsEntier ;;
